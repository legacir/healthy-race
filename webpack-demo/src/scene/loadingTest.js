import Phaser from 'phaser';
export default class extends Phaser.Scene
{
	constructor ()
	{
		super({ key: 'Loading'});
	}

	preload ()
	{
	var progressBar = this.add.graphics();
    var progressBox = this.add.graphics();
    var width = this.cameras.main.width;
    var height = this.cameras.main.height;
    progressBox.fillStyle(0x222222, 0.8);
    progressBox.fillRect(1350/3, 900/3, 470, 50);
    var loadingText = this.make.text({
        x: width/2,
        y: (height/2)-200,
        text: 'Chargement...',
        style: {
            font: '20px monospace',
            fill: '#ffffff'
        }
    });
    loadingText.setOrigin(0.5,0.5);

    var percentText = this.make.text({
        x: width / 2,
        y : height / 2-5,
        text: '0%',
        style: {
            font: '18px monospace',
            fill: '#ffffff'
        }
    });
    percentText.setOrigin(0.5,0.5);

    var assetText = this.make.text({
        x: width / 2,
        y: height /2+50,
        text: '',
        style: {
            font: '18px monospace',
            fill: '#ffffff'
        }
    });
    assetText.setOrigin(0.5,0.5);

    this.load.on('progress', function (value) {
    console.log(value);
    progressBar.clear();
    progressBar.fillStyle(0xffffff, 1);
    progressBar.fillRect(1360/3, 910/3, 450 * value, 30);
    percentText.setText(parseInt(value * 100) + '%');
    });
            
    this.load.on('fileprogress', function (file) {
    console.log(file.src);
    assetText.setText('Chargement de: ' + file.key);
    });
 
    this.load.on('complete', function () {
    console.log('complete');
    progressBar.destroy();
    progressBox.destroy();
    loadingText.destroy();
    percentText.destroy();
    assetText.destroy();
    });

    setTimeout(etape1,2000);
    function etape1(){
    this.load.image('sky', 'assets/sky.png');	
    }
    setTimeout(etape2,3000);
    function etape2(){
    this.load.image('ground', 'assets/platform.png');	
    }
    setTimeout(etape3,4000);
    function etape3(){
    this.load.image('star', 'assets/star.png');
	}
    setTimeout(etape4,5000);
    function etape4(){
    this.load.image('bomb', 'assets/bomb.png');
	}
	setTimeout(etape5,6000);
	function etape5(){
    this.load.image('maison', 'assets/maison.png');
	}
	setTimeout(etape6, 7000);
	function etape6(){
    this.load.spritesheet('dude', 
        'assets/dude.png',
        { frameWidth: 32, frameHeight: 48 }
    );
	}
	}
	update (){
		setTimeout(ldlevel,2000);
		function ldlevel(){
		this.scene.start('level 0');
	}
	}
}