# Healthy Race

Projet de CIR2 dans le cadre des CIRIOUSGAME de l'ISEN LILLE.

 _Thématique_ : Alimentation équilibrée
 
## __A. FICHE PRODUIT__

 - Genre : Aventure
  - Style : plateforme
  - Moteur : Phaser
  - 1 joueur

  note : le jeu a été codé pour un rendu en 1300x900, des problèmes d'affichages peuvent donc apparaitre.

## __B. PRINCIPE DU JEU__ 

### __Description__ :  

Bienvenue sur Healthy-race !

Nous vous proposons un jeu sur le thème de l'alimentation équilibré. Pour finir nos 3 parcours vous devrez récupérer assez de vitamines tout en évitant les matières grasses ! Mais attention à votre jauge de calories ! Si elle tombe à 0 vos mouvements deviendront compliqués.

Faites également attention aux tomates OGM et aux donuts sur pattes ! Ils vous feront perdre la partie. Et pour compliquer tout ça un temps imparti vous est fixé, elle est pas belle la vie ?




### Lancement du jeu :

Attention : npm est nécessaire pour démarrer le serveur en local.

Se rendre dans le dossier healthy-race/webpack-demo/
Puis utiliser la commande : npm run serve