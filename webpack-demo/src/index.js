                        /*Version V1.4 */

import Phaser from "phaser"; // Importation de phaser
import Loading from './scene/loading';
import Menu from './scene/startMenu';
import Level0 from './scene/level 0';
import Level1 from './scene/level 1';
import Level2 from './scene/level 2';
//import Level3 from './scene/level 3';
var config = {              //Configuration de base du système de jeu
    type: Phaser.AUTO,
    width: 1350,
    height: 900,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 300 },
            debug: false
        }
    },
    audio: {
        disableWebAudio: true
    },
    scene: [Loading,Menu, Level0, Level1, Level2]
};

var game = new Phaser.Game(config);