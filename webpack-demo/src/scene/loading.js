import Phaser from 'phaser';

export default class extends Phaser.Scene
{
    constructor ()
    {
        super({ key: 'Loading'});
    }

    preload ()
    {
    /*Barre de chargement */
    this.loadingFinish = false;
    this.done = false;
    this.progressBar = this.add.graphics();
    this.progressBox = this.add.graphics();
    var width = 1350;
    var height = 900;
    this.progressBox.fillStyle(0x222222, 0.8);
    this.progressBox.fillRect(1350/3, 880/3, 470, 50);
    this.loadingText = this.make.text({
        x: width/2,
        y: (height/2)-200,
        text: 'Chargement...',
        style: {
            font: '20px monospace',
            fill: '#ffffff'
        }
    });
    this.loadingText.setOrigin(0.5,0.5);

    this.percentText = this.make.text({
        x: width / 2,
        y : height / 2-5,
        text: '0%',
        style: {
            font: '18px monospace',
            fill: '#ffffff'
        }
    });
    this.percentText.setOrigin(0.5,0.5);

    this.assetText = this.make.text({
        x: width / 2,
        y: height /2+50,
        text: '',
        style: {
            font: '18px monospace',
            fill: '#ffffff'
        }
    });
    this.assetText.setOrigin(0.5,0.5);

    this.load.on('progress', (value) => {
    console.log(value);
    this.progressBar.clear();
    this.progressBar.fillStyle(0xffffff, 1);
    this.progressBar.fillRect(1360/3, 910/3, 450 * value, 30);
    this.percentText.setText(parseInt(value * 100) + '%');
    });
            
    this.load.on('fileprogress', (file) => {
    console.log(file.src);
    this.assetText.setText('Chargement de: ' + file.key);
    });
 
    this.load.on('complete', () => {
    console.log('complete');
    this.loadingFinish = true;
    });
            /*élements à charger*/

    this.load.audio('musiqueMenu', 'assets/musiqueMenu.mp3');

    this.load.image('titre', 'assets/titre.png');
    
    this.load.image('sky', 'assets/sky.png');
    this.load.image('arrièreplan1','assets/arrièreplan1.png');
    this.load.image('Montagnes', 'assets/Montagnes.jpg');
    this.load.image('MontagnesS', 'assets/MontagnesS.jpg');
    this.load.image('MontagnesS2', 'assets/MontagnesS2.jpg');
    this.load.image('Desert', 'assets/Desert.jpg');
    this.load.image('maison', 'assets/maison.png');
    this.load.image('départ', 'assets/départ.png');
    this.load.image('exit','assets/exit.png');
    this.load.image('doorClosedmid','assets/doorClosedmid.png');
    this.load.image('doorClosedtop', 'assets/doorClosedtop.png');
    this.load.image('doorOpenmid', 'assets/doorOpenmid.png');
    this.load.image('doorOpentop', 'assets/doorOpentop.png');

    this.load.image('ground', 'assets/platform.png');
    this.load.image('sol','assets/sol.png');
    this.load.image('sol2', 'assets/sol2.png');
    this.load.image('solcassé','assets/solcassé.png');
    this.load.image('terre', 'assets/terre.png');
    this.load.image('bordureD', 'assets/bordureD.png');
    this.load.image('bordureG', 'assets/bordureG.png');
    this.load.image('solS','assets/snowMid.png');
    this.load.image('terreS', 'assets/snowCenter.png');
    this.load.image('bordureGS', 'assets/snowLeft.png');
    this.load.image('bordureDS','assets/snowRight.png');
    this.load.image('solFS', 'assets/snowHalf.png');
    this.load.image('solD', 'assets/sandMid.png');
    this.load.image('terreD', 'assets/sandCenter.png');
    this.load.image('bordureGD', 'assets/sandLeft.png');
    this.load.image('bordureDD', 'assets/sandRight.png');
    this.load.image('solFD', 'assets/sandHalf.png');

    this.load.image('star', 'assets/star.png');
    this.load.image('bomb', 'assets/bomb.png');
    this.load.image('pics', 'assets/pics.png');
    this.load.image('boxItem', 'assets/boxItem.png');
    this.load.image('boxItem_disabled', 'assets/boxItem_disabled.png');
    this.load.image('keyRed', 'assets/keyRed.png');
    this.load.image('boxEnemy','assets/boxExplosive.png');
    this.load.image('boxEnemyUsed','assets/boxExplosive_disabled.png');

    this.load.image('ananas', 'assets/ananas.png');
    this.load.image('asperge', 'assets/asperge.png');
    this.load.image('banane', 'assets/banane.png');
    this.load.image('cerise', 'assets/cerise.png');
    this.load.image('citron', 'assets/citron.png');
    this.load.image('fraise', 'assets/fraise.png');
    this.load.image('raisin1', 'assets/raisin1.png');
    this.load.image('radis','assets/radis.png');
    this.load.image('salade', 'assets/salade.png');
    this.load.image('poireau', 'assets/poireau1.png');

    this.load.image('frites', 'assets/frites.png');
    this.load.image('hamburger', 'assets/hamburger.png');
    this.load.image('hotdog', 'assets/hotdog.png');
    this.load.image('pizza', 'assets/pizza.png');
    this.load.image('sandwich', 'assets/sandwich.png');
    this.load.image('carreMenu', 'assets/carreGris.png');



    this.load.image('bPlay1', 'assets/bPlay1.png');
    this.load.image('bOption', 'assets/bOption.png');
    this.load.image('bCredit', 'assets/bCredit.png');

    this.load.image('bHome', 'assets/bHome.png');
    this.load.image('interrogation', 'assets/interrogation.png');
    this.load.image('gameover', 'assets/gameover.png');
    this.load.image('restart', 'assets/restart.png');
    this.load.image('victory', 'assets/victory.png');
    this.load.image('next', 'assets/next.png');
    this.load.image('bienvenue', 'assets/affichebienvenue.png');
    this.load.image('objectifLevel0', 'assets/objectifLevel0.png');
    this.load.image('resultatTab','assets/resultattab.png');
    this.load.image('PersoPret', 'assets/PersoPret.png');


    this.load.image('pleinEcran','assets/pleinEcran.png');



    this.load.spritesheet('enemy','assets/tomatohead1.png', {frameWidth:40, frameHeight:39});
    this.load.spritesheet('donut','assets/Donut.png', {frameWidth:50.75, frameHeight:60});




    this.load.spritesheet('PersoV4', 
        'assets/PersoV4.png',
        { frameWidth: 54.7, frameHeight: 72 } //Taille des frames
    );
    this.load.spritesheet('PersoAV1', 'assets/PersoAV1.png',{frameWidth:30, frameHeight: 53});
    }
    update (){

        if (this.loadingFinish && !this.done) {
            setTimeout(() => {
                this.progressBar.destroy();
                this.progressBox.destroy();
                this.loadingText.destroy();
                this.percentText.destroy();
                this.assetText.destroy();
                this.scene.start('startMenu');
                }
                ,1000);

            this.done = true;
        }


    }
}