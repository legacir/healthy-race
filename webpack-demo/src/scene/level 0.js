import Phaser from 'phaser';


//DÉBUT Déclaration des variables
var platforms;
var player;
var cursors;
var fruits;
var BadFood;
var score = 0;
var scoreText;
var BonusFinal = 0;
var BonusACT = 0;
var star;
var timer;
var TimeInfo;
var TimerOn = 0;
var chrono = 50000;
var gameEnd;
var bombs;
var maison;
var pics;
var départ;
var vitamines = 0;
var matieresG = 0;
var calories = 0;
var vitaminesCounter;
var matièresGCounter;
var caloriesCounter;
var box;
var boxUsed;
var key;
var keyFound = 0;
var enemy;
var donut;
var doorClosed;
var doorOpen;
var vitaminesR;
var matieresGR;
var caloriesR;

//FIN Déclaration des variables

export default class extends Phaser.Scene
{
	constructor (){
		super({key: 'level 0'});
	}
	preload(){}
	
	create () // Cette fonction regroupe tout l'algorithme de création du jeu 
	{

		

		//DÉBUT fond d'écran
		for(var i = 1; i < 15 ; i = i+2.8){
	    this.add.image(650*i,250,'Montagnes'); // on ajoute l'image commme arrière plan
	    //this.add.image(150+(650 * (i-1)),613,'maison').setScale(0.35);
		}
		//FIN Fond d'écran

		//DÉBUT PLATEFORME
	    platforms = this.physics.add.staticGroup(); // on crée les plateformes comme étant un groupe fixe c'est à dire que ça ne bougera absolument pas durant le jeu
	    for(var i =68; i <= 1300; i = i+128){
	    	platforms.create(i, 778, 'sol2'); // on ajoute une plateforme avec des coordonnées et une image et ici on double la taille
		}

		platforms.create(500, 650, 'solcassé').setScale(0.8).refreshBody();
	    platforms.create(700, 500, 'solcassé').setScale(0.8).refreshBody();
	    platforms.create(900, 400, 'solcassé').setScale(0.8).refreshBody();
	    platforms.create(1100,350, 'solcassé').setScale(0.8).refreshBody();
	    /*Pente 1*/
		for(var i = 1300; i <= 1812; i = i+128){
			for(var j = 700; j <= 700 + 128; j = j + 128){
	    	platforms.create(i, j, 'terre');
	    }
		}
		 for(var k = 1300; k <= 1812; k = k+128){
	    	platforms.create(k,700-128,'sol2');
	    }
	    /*easter egg*/
	    platforms.create(1812 + 128,354,'bordureG');
	    for(i = 2068; i <= 2580; i = i + 128){
	    	platforms.create(i,354,'sol2');
	    }
	    for(j = 354; j >= -30; j -= 128){
	    	platforms.create(2580 + 128, j, 'terre');
	    }

	    /*pente 2*/
	    for(var i = 1812 + 128; i <= 2452; i = i+128){
			for(var j = 800; j <= 800 + 128; j = j + 128){
	    	platforms.create(i, j, 'terre');
	    }
		}
		 for(var k = 1812+128; k <= 2452; k = k+128){
	    	platforms.create(k,800-128,'sol2');
	    }
	    /*Sol final*/
	    for(var i =2452+128; i <= 3604; i = i+128){
	    	platforms.create(i, 778, 'sol2'); // on ajoute une plateforme avec des coordonnées et une image et ici on double la taille
		}
		



	     /*Platformes de fin*/
	    for(var i = 3604 + 128; i < 4500; i = i+128){
			for(var j = 500; j <= 1012; j = j + 128){
	    	platforms.create(i, j, 'terre');
	    }
		}
		 for(var k = 3604 + 128; k < 4500; k = k+128){
	    	platforms.create(k,500-128,'sol2');
	    }
	    //FIN Plateforme

	    //DEBUT Physique de jeu
	    départ = this.physics.add.staticGroup();
	    départ.create(300,680,'départ').setScale(0.6);
	    this.add.image(3800, 249, 'exit');

	    pics = this.physics.add.staticGroup();
	    for(var i = 2452 + 128; i <= 3604; i = i + 240){
	    	pics.create(i,695,'pics').setScale(0.6).refreshBody();
	    }

	    box = this.physics.add.staticGroup();
	    box.create(2200,100,'boxItem').setScale(0.5).refreshBody();
	    boxUsed = this.physics.add.staticGroup();
	    key = this.physics.add.group();
	    this.star = this.physics.add.group();

	    doorClosed = this.physics.add.staticGroup();
	    doorClosed.create(4100,265, 'doorClosedmid').setScale(0.7);
	    doorClosed.create(4100,197,'doorClosedtop').setScale(0.7);
		doorOpen = this.physics.add.staticGroup();
	    //Fin Physique du jeu

	    //DEBUT Déplacement
	    cursors = this.input.keyboard.createCursorKeys();
	    cursors = this.input.keyboard.addKeys('Z,Q,D,enter,space,left,right'); // on crée les curseurs de déplacements
	    //FIN Déplacement

	    //DÉBUT Compteur
	    vitaminesCounter = this.add.text(120,75, 'vitamines: 0', { fontSize: '28px Calibri'}).setScrollFactor(0);  
	    matièresGCounter = this.add.text(120,100, 'matieresG: 0', { fontSize: '28px Calibri'}).setScrollFactor(0);
	    this.caloriesCounter = this.add.text(120,130, 'calories: 0', { fontSize: '28px Calibri'}).setScrollFactor(0);
	    //FIN Compteur

	    

	    //DÉBUT Gestion perso
	    player = this.physics.add.sprite(100,600,'PersoV4'); // On crée le joueur 'sprite'
	    player.setBounce(0.2); // On paramètre le rebondissement lorsqu'il touche le sol
	    player.setCollideWorldBounds(false); // On dit que le personnage ne peut pas aller en dehors de la fenêtre 
	    player.body.setGravityY(500); //On lui fixe sa propre gravité
	    //player.body.setSize()
	    //FIN Gestion perso

	    this.physics.add.collider(player, platforms); // On précise que le personnage doit entrer en collision avec les plateformes
	    this.physics.add.collider(player,box,boxkey,null, this);
	    this.physics.add.collider(player,boxUsed);
	    this.physics.add.collider(key,platforms);
	    this.physics.add.collider(player,doorClosed,doorUnlock,null,this);
	    this.physics.add.collider(player,doorOpen,victory,null,this);
	    this.physics.add.collider(this.star,platforms,starAnim,null,this);
	    this.physics.add.overlap(player,pics, gameOver, null, this);
	    this.physics.add.overlap(player,départ,startChrono,null,this);
	    this.physics.add.overlap(player,key,doorkey,null,this);
	    this.physics.add.overlap(player,this.star,Bonus,null,this);

	    //DEBUT ennemies
	    enemy = this.physics.add.sprite(400,400, 'enemy' );
		enemy.setBounce(0.2);
		enemy.setCollideWorldBounds(false);
		enemy.body.setGravityY(500);
		enemy.setVelocityX(240);
		this.physics.add.collider(enemy, platforms);
		enemy.setCollideWorldBounds(false);
		this.physics.add.overlap(player,enemy,gameOver, null, this);

	    donut = this.physics.add.sprite(2000,500, 'donut' );
		donut.setBounce(0.2);
		donut.setCollideWorldBounds(false);
		donut.body.setGravityY(500);
		donut.setVelocityX(140);
		this.physics.add.collider(donut, platforms);
		donut.setCollideWorldBounds(false);
		this.physics.add.overlap(player,donut,gameOver, null, this);
		//Fin ennemies

		//DEBUT Caméra
	    this.cameras.main.setBounds(0, 0, 4350, 900);
	    this.cameras.main.startFollow(player);
	    this.cameras.main.setZoom(1.2);
	    //FIN Caméra

	    //Ici on crée 3 animations pour le déplacement du perso
	    this.anims.create({
	    key: 'walk',
	    frames: [
                    { key: 'PersoV4', frame: 0 },
                    { key: 'PersoV4', frame: 1 },
                    { key: 'PersoV4', frame: 2 },
                    { key: 'PersoV4', frame: 3 },
                    { key: 'PersoV4', frame: 2 },
                    { key: 'PersoV4', frame: 1 },
                ],
	    frameRate: 14,
	    repeat: -1
	});

	this.anims.create({
	    key: 'turn',
	    frames: [ { key: 'PersoV4', frame: 1 } ],
	    frameRate: 10
	});

	this.anims.create({
		key: 'run',
		frames: this.anims.generateFrameNumbers('enemy', { start: 0, end: 3}),
		frameRate: 13
	});
	this.anims.create({
		key: 'avance',
		frames: this.anims.generateFrameNumbers('donut', { start: 0, end: 3}),
		frameRate: 13
	});

	

	/*this.anims.create({
	    key: 'right',
	    frames: this.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
	    frameRate: 10,
	    repeat: -1
	});*/


	//On crée les fruits à récupérer
	fruits = this.physics.add.group();
	    //key: ['cerise','fraise','asperge','citron'],
	    //repeat: 3,
	    //setXY: { x: 350, y:0, stepX: Phaser.Math.Between(200, 500)}
	//});
	fruits.create(400,0,'raisin1');
	fruits.create(500,0,'cerise');
	fruits.create(700,0,'fraise');
	fruits.create(900,0,'asperge');
	fruits.create(1100,0,'citron');
	fruits.create(2100,0,'cerise');
	fruits.create(2300,0,'ananas');
	fruits.children.iterate(function(child){
	    child.setBounceY(Phaser.Math.FloatBetween(0.2, 0.4));
	});
	this.physics.add.collider(fruits, platforms);
	this.physics.add.overlap(player, fruits, collectFruits, null, this);

	BadFood = this.physics.add.group();
	    //key: ['hamburger', 'sandwich', 'frites'],
	    //repeat: 1,
	    //setXY: { x: 1000, y:0, stepX: Phaser.Math.Between(1000, 2000)}
	//});
	BadFood.create(1300,0,'hamburger');
	BadFood.create(1400,0,'sandwich');
	BadFood.create(1700,0,'frites');
	BadFood.create(2180,0,'hamburger');
	BadFood.children.iterate(function(child){
	    child.setBounceY(Phaser.Math.FloatBetween(0.2, 0.4));
	});
	this.physics.add.collider(BadFood, platforms);
	this.physics.add.overlap(player, BadFood, collectBadFood, null, this);

	//BIENVENUE
	let welcome = this.add.image(670, 380, 'bienvenue').setInteractive().setScrollFactor(0).on('pointerdown', () => {
            welcome.destroy();
            let objectifs = this.add.image(670, 380, 'objectifLevel0').setInteractive().setScrollFactor(0).on('pointerdown', () => {
            objectifs.destroy();
        });
        });

	function collectFruits (player, fruits){
	    fruits.disableBody(true,true);
	    vitamines += 100;
	    calories += 100;
	    vitaminesCounter.setText('vitamines: '+ vitamines);
	    matièresGCounter.setText('matieresG:'+ matieresG);
	    this.caloriesCounter.setText('calories :'+ calories);
	    /*if (fruits.countActive(true) === 0)
	    {
	        fruits.children.iterate(function (child) {

	            child.enableBody(true, child.x, 0, true, true);

	        });
.setScale(0.5).refreshBody();
	        var x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

	        var bomb = bombs.create(x, 16, 'bomb');
	        bomb.setBounce(1);
	        bomb.setCollideWorldBounds(true);
	        bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);

	    }*/
	}
	function collectBadFood (player,BadFood){
		BadFood.disableBody(true,true);
	    matieresG += 100;
	    calories += 500;
	    vitaminesCounter.setText('vitamines: '+ vitamines);
	    matièresGCounter.setText('matieresG :'+ matieresG);
	    this.caloriesCounter.setText('calories :'+ calories);
	}
	function startChrono(player){
		if (TimerOn == 0) {
		this.timer = this.time.addEvent({ delay: chrono, callback: gameOver, callbackScope: this });
	    this.TimeInfo = this.add.text(630, 80, '',{ font: '32px Calibri'}).setScrollFactor(0);
	    TimerOn = 1;
	}
	}
	function boxkey(player,box){
		box.disableBody(true, true);
		boxUsed.create(2200,100,'boxItem_disabled').setScale(0.5).refreshBody();
		key.create(2350,0,'keyRed');
	}
	function doorkey(player,key){
		key.disableBody(true, true);
		keyFound = 1;
	}
	function doorUnlock(player,doorClosed){
		if (keyFound == 1) {
			doorClosed.disableBody(true,true);
			doorOpen.create(4100,265, 'doorOpenmid').setScale(0.7);
	    	doorOpen.create(4100,197,'doorOpentop').setScale(0.7);
		}
	}
	function starAnim(star,platforms){
		this.star.setVelocityY(-200);
	}
	function Bonus(player,star){
		star.disableBody(true,true);
		BonusFinal = 1;
	}
	function gameOver(){	
		this.physics.pause();
	    player.setTint(0xff0000);
	    player.anims.play('turn');
	    gameOver = true;
	    this.timer.paused = true;
	    this.add.image(650, 300,'gameover').setScale(0.8).setScrollFactor(0);
        let bouton = this.add.image(650, 380, 'bHome').setScale(0.08).setInteractive().setScrollFactor(0).on('pointerdown', () => {
            location.reload();
        });
	   }
	function victory(){
		this.physics.pause();
		this.timer.paused = true;
		vitaminesR = this.add.text(700,340, vitamines, { fontSize: '48px Calibri'}).setScrollFactor(0).setDepth(100);
		matieresGR = this.add.text(750,410, matieresG,{fontSize: '48px Calibri'}).setScrollFactor(0).setDepth(100);
		caloriesR = this.add.text(700,500, calories, {fontSize: '48px Calibri'}).setScrollFactor(0).setDepth(100);
		let resultats = this.add.image(700, 400, 'resultatTab').setInteractive().setScrollFactor(0).on('pointerdown', () => {
            resultats.destroy();
            vitaminesR.destroy();
            matieresGR.destroy();
            caloriesR.destroy();
            let victoire = this.add.image(650, 300,'victory').setScale(0.8).setScrollFactor(0);
            let bouton = this.add.image(650, 380, 'next').setInteractive().setScrollFactor(0).on('pointerdown', () => {
            this.scene.start('level 1');
        });
        });

		/*this.add.image(650, 300,'victory').setScale(0.8).setScrollFactor(0);
        let bouton = this.add.image(650, 380, 'next').setInteractive().setScrollFactor(0).on('pointerdown', () => {
            this.scene.start('level 1');
        });*/
	}

	/*bombs = this.physics.add.group();
	this.physics.add.collider(player, bombs, hitBomb, null, this);
	this.physics.add.collider(bombs, platforms);

	function hitBomb (player, bomb){
	    this.physics.pause();
	    player.setTint(0xff0000);
	    player.anims.play('turn');
	    gameOver = true;
	}*/
	}

	update ()
	{

	/*Animations IA*/
	//console.log(donut.x);
	enemy.anims.play('run', true);
	donut.anims.play('avance', true);

	if(enemy.x == 580){
		enemy.setVelocityX(240);
		enemy.flipX = false;
	}


	if(enemy.x == 1216){
		enemy.setVelocityX(-240);
		enemy.flipX = true;
	}
	if(donut.x < 1950){
		donut.setVelocityX(150);
		donut.flipX = false;
	}


	if(donut.x > 2500){
		donut.setVelocityX(-150);
		donut.flipX = true;
	}

	/*Gestion de l'étoile bonus et de son animation*/
	if (vitamines >= 600 && BonusACT == 0) {
	    	this.star.create(3400,600,'star');
	    	BonusACT = 1;
	    }
	/*if (this.star.body.touching.down) {
		this.star.setVelocity(-100);
	}*/

	/*Gestion timer*/
	if(this.TimeInfo){
	this.TimeInfo.setText('Time: ' + Math.floor(chrono - this.timer.getElapsed())/1000);
		if (chrono - this.timer.getElapsed() < 10) {
			this.TimeInfo.setFill('#FF0921');
		}
		else{
			this.TimeInfo.setFill('#000');
		}
	}

	/*Gestion des objectifs*/
	if (vitamines >= 600) {
		vitaminesCounter.setFill('#34C924');
	}
	else if (vitamines != 0 && vitamines < 600) {
		vitaminesCounter.setFill('#CC5500');
	}
	else if (vitamines == 0) {
		vitaminesCounter.setFill('#FF0921');
	}
	if (matieresG > 300) {
		matièresGCounter.setFill('#FF0921');
	}
	else if (matieresG <= 300) {
		matièresGCounter.setFill('#34C924');
	}
	if (calories > 5000) {
		this.caloriesCounter.setFill('#FF0921');
	}
	else if (calories != 0 && calories <= 5000) {
		this.caloriesCounter.setFill('#34C924');
	}
	else if (calories == 0) {
		this.caloriesCounter.setFill('#FF0921');
	}
	/*Gestion déplacements*/
		if (vitamines >= 0 && vitamines < 300 && calories != 0){
			if (cursors.Q.isDown || cursors.left.isDown)
			{
			    player.setVelocityX(-240);

			    player.anims.play('walk', true);

			    player.flipX = true;
			    calories -= 1;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else if (cursors.D.isDown || cursors.right.isDown)
			{
			    player.setVelocityX(240);

			    player.anims.play('walk', true);

			    player.flipX = false;

			    calories -= 1;
			  if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else
			{
			    player.setVelocityX(0);

			    player.anims.play('turn');
			}

			if (cursors.space.isDown && player.body.touching.down || cursors.Z.isDown && player.body.touching.down)
			{
			    player.setVelocityY(-500);
			    calories -= 5;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
		}

		else if (vitamines >= 300 && BonusFinal == 0 && calories != 0){
			if (cursors.Q.isDown || cursors.left.isDown)
			{
			    player.setVelocityX(-340);

			    player.anims.play('walk', true);

			    player.flipX = true;
			    calories -= 1;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else if (cursors.D.isDown || cursors.right.isDown)
			{
			    player.setVelocityX(340);

			    player.anims.play('walk', true);

			    player.flipX = false;

			    calories -= 1;
			  if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else
			{
			    player.setVelocityX(0);

			    player.anims.play('turn');
			}

			if (cursors.space.isDown && player.body.touching.down || cursors.Z.isDown && player.body.touching.down)
			{
			    player.setVelocityY(-600);
			    calories -= 5;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
		}

		 else if (BonusFinal == 1){
			if (cursors.Q.isDown || cursors.left.isDown)
			{
			    player.setVelocityX(-340);

			    player.anims.play('walk', true);

			    player.flipX = true;
			    calories -= 1;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else if (cursors.D.isDown || cursors.right.isDown)
			{
			    player.setVelocityX(340);

			    player.anims.play('walk', true);

			    player.flipX = false;

			    calories -= 1;
			  if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else
			{
			    player.setVelocityX(0);

			    player.anims.play('turn');
			}

			if (cursors.space.isDown && player.body.touching.down || cursors.Z.isDown && player.body.touching.down)
			{
			    player.setVelocityY(-830);
			    calories -= 5;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
		}


		else{
			if (cursors.Q.isDown || cursors.left.isDown)
			{
			    player.setVelocityX(-200);

			    player.anims.play('walk', true);

			    player.flipX = true;

			    calories -= 1;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else if (cursors.D.isDown || cursors.right.isDown)
			{
			    player.setVelocityX(200);

			    player.anims.play('walk', true);

			    player.flipX = false;

			    calories -= 1;

			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}

			}
			else
			{
			    player.setVelocityX(0);

			    player.anims.play('turn');
			}

			if (cursors.space.isDown && player.body.touching.down || cursors.Z.isDown && player.body.touching.down)
			{
			    player.setVelocityY(-300);

			    calories -= 5;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}			
		}
	}
}