import Phaser from 'phaser';


//DÉBUT Déclaration des variables
var platforms;
var player;
var cursors;
var fruits;
var BadFood;
var score = 0;
var scoreText;
var BonusFinal = 0;
var BonusACT = 0;
var star;
var timer;
var TimeInfo;
var TimerOn = 0;
var chrono = 50000;
var gameEnd;
var bombs;
var maison;
var pics;
var départ;
var vitamines = 0;
var matièresG = 0;
var calories = 0;
var vitaminesCounter;
var matièresGCounter;
var caloriesCounter;
var box;
var boxUsed;
var boxEnemy;
var boxEnemyUsed;
var key;
var keyFound = 0;
var enemy;
var doorClosed;
var doorOpen;
var vitaminesR;
var matieresGR;
var caloriesR;
//FIN Déclaration des variables

export default class extends Phaser.Scene
{
	constructor (){
		super({key: 'level 1'});
	}
	preload(){}
	
	create () // Cette fonction regroupe tout l'algorithme de création du jeu 
	{

		

		//DÉBUT fond d'écran
		//for(var i = 1; i < 15 ; i = i+4){
	    this.add.image(650,450,'MontagnesS').setScale(0.9).setScrollFactor(0); // on ajoute l'image commme arrière plan
	    //this.add.image(150+(650 * (i-1)),613,'maison').setScale(0.35);
		//}
		//FIN Fond d'écran

		//DÉBUT PLATEFORME
	    platforms = this.physics.add.staticGroup(); // on crée les plateformes comme étant un groupe fixe c'est à dire que ça ne bougera absolument pas durant le jeu

	    /*Pente 1*/
		for(var i = 64; i <= 576; i = i+128){
			for(var j = 500; j <= 1012 + 128; j = j + 128){
	    	platforms.create(i, j, 'terreS');
	    }
		}
		 for(var k = 64; k <= 576; k = k+128){
	    	platforms.create(k,500-128,'solS');
	    }
		for(var i = 576 + 128; i <= 960; i = i+128){
			for(var j = 628; j <= 856; j = j + 128){
	    	platforms.create(i, j, 'terreS');
	    }
		}
		 for(var k = 576 + 128; k <= 960; k = k+128){
	    	platforms.create(k,628-128,'solS');
	    }
		for(var i = 960 + 128; i <= 1344; i = i+128){
			for(var j = 756; j <= 1012 ; j = j + 128){
	    	platforms.create(i, j, 'terreS');
	    }
		}
		 for(var k = 960 + 128; k <= 1344; k = k+128){
	    	platforms.create(k,756-128,'solS');
	    }
		for(var i = 1344 + 128; i <= 2624; i = i+128){
			for(var j = 884; j <= 1012 ; j = j + 128){
	    	platforms.create(i, j, 'terreS');
	    }
		}
		 for(var k = 1344 + 128; k <= 3648; k = k+128){
	    	platforms.create(k,884-128,'solS');
	    }
	    /*Pente 2*/
	    platforms.create(1600,500,'solFS');
	    platforms.create(1800,400,'solFS');
	    platforms.create(2200,354,'bordureGS');
	    for(i = 2200 + 128; i <= 2968; i = i + 128){
	    	platforms.create(i,354,'solS');
	    }
	    for(j = 354; j >= -30; j -= 128){
	    	platforms.create(2968 + 128, j, 'terreS');
	    }

	    /*Pente 3*/
	    platforms.create(3500,550,'bordureGS');
	    for(i = 3500 + 128; i <= 3884; i = i + 128){
	    	platforms.create(i,550,'solS');
	    }
	    for(i = 3756; i <= 3884; i = i + 128){
	    	for(j = 678; j <= 934; j = j + 128){
	    		platforms.create(i,j,'terreS');
	    	}
	    }

	    /*Sol milieu*/
	    for(i = 4012; i <= 4908; i = i + 128){
	    	platforms.create(i,800,'solS');
	    }
	    platforms.create(4100, 500, 'solFS').setScale(0.8).refreshBody();
	    platforms.create(4400, 500, 'solFS').setScale(0.8).refreshBody();
	    platforms.create(4700, 500, 'solFS').setScale(0.8).refreshBody();

	     /*Platformes de fin*/
	    for(i = 4908; i <= 5292; i = i + 128){
	    	for(j = 678; j <= 934; j = j + 128){
	    		platforms.create(i,j,'terreS');
	    	}
	    }
	    for(i = 4908; i <= 5420 ; i = i + 128){
	    	platforms.create(i,550,'solS');
	    }
	   	platforms.create(5548,550,'bordureDS');

	   	/*sol fin*/
	   	for(i = 5420; i<= 5676; i = i + 128){
	   		platforms.create(i,800,'solS');
	   	}
	   	for(i = 6300; i <= 6600; i = i +128){
	   		platforms.create(i,800,'solS');
	   	}

	    //FIN Plateforme

	    //DEBUT Physique de jeu
	    départ = this.physics.add.staticGroup();
	    départ.create(300,270,'départ').setScale(0.6);
	    this.add.image(6250, 672, 'exit');

	    pics = this.physics.add.staticGroup();
	    for(var i = 4000 ; i <= 4788; i = i + 128){
	    	pics.create(i,718,'pics').setScale(0.6).refreshBody();
	    }
	    /* Si le joueur tombe dans le vide */
	    for(var i = 5700; i <= 6300; i = i + 90){
	    	pics.create(i,900,'pics').setScale(0.6).refreshBody();
	    }

	    box = this.physics.add.staticGroup();
	    box.create(3600,300,'boxItem').setScale(0.5).refreshBody();
	    boxUsed = this.physics.add.staticGroup();
	    boxEnemy = this.physics.add.staticGroup();
	    boxEnemy.create(2400,100,'boxEnemy').setScale(0.5).refreshBody();
	    boxEnemyUsed = this.physics.add.staticGroup();
	    key = this.physics.add.group();
	    this.star = this.physics.add.group();

	    doorClosed = this.physics.add.staticGroup();
	    doorClosed.create(6400,692, 'doorClosedmid').setScale(0.7);
	    doorClosed.create(6400,624,'doorClosedtop').setScale(0.7);
		doorOpen = this.physics.add.staticGroup();
	    //Fin Physique du jeu

	    //DEBUT Déplacement
	    cursors = this.input.keyboard.createCursorKeys();
	    cursors = this.input.keyboard.addKeys('Z,Q,D,enter,space,left,right'); // on crée les curseurs de déplacements
	    //FIN Déplacement

	    //DÉBUT Compteur
	    vitaminesCounter = this.add.text(120,75, 'vitamines: 0', { fontSize: '28px Calibri', fill: '#FFFAFA'}).setScrollFactor(0);  
	    matièresGCounter = this.add.text(120,100, 'matièresG: 0', { fontSize: '28px Calibri', fill: '#FFFAFA'}).setScrollFactor(0);
	    this.caloriesCounter = this.add.text(120,130, 'calories: 0', { fontSize: '28px Calibri', fill: '#FFFAFA'}).setScrollFactor(0);
	    //FIN Compteur


	    //DÉBUT Gestion perso
	    player = this.physics.add.sprite(100,200,'PersoV4'); // On crée le joueur 'sprite'
	    player.setBounce(0.2); // On paramètre le rebondissement lorsqu'il touche le sol
	    player.setCollideWorldBounds(false); // On dit que le personnage ne peut pas aller en dehors de la fenêtre 
	    player.body.setGravityY(500); //On lui fixe sa propre gravité
	    //player.body.setSize()
	    //FIN Gestion perso

	    this.physics.add.collider(player, platforms); // On précise que le personnage doit entrer en collision avec les plateformes
	    this.physics.add.collider(player,box,boxkey,null, this);
	    this.physics.add.collider(player,boxUsed);
	    this.physics.add.collider(key,platforms);
	    this.physics.add.collider(player,doorClosed,doorUnlock,null,this);
	    this.physics.add.collider(player,doorOpen,victory,null,this);
	    this.physics.add.collider(this.star,platforms,starAnim,null,this);
	    this.physics.add.collider(player,boxEnemy,EnemyAPP,null, this);
	    this.physics.add.collider(player,boxEnemyUsed);
	    this.physics.add.overlap(player,pics, gameOver, null, this);
	    this.physics.add.overlap(player,départ,startChrono,null,this);
	    this.physics.add.overlap(player,key,doorkey,null,this);
	    this.physics.add.overlap(player,this.star,Bonus,null,this);

		//DEBUT Caméra
	    this.cameras.main.setBounds(0, 0, 6500, 900);
	    this.cameras.main.startFollow(player);
	    this.cameras.main.setZoom(1.2);
	    //FIN Caméra

	    //Ici on crée 3 animations pour le déplacement du perso
	    this.anims.create({
	    key: 'walk',
	    frames: [
                    { key: 'PersoV4', frame: 0 },
                    { key: 'PersoV4', frame: 1 },
                    { key: 'PersoV4', frame: 2 },
                    { key: 'PersoV4', frame: 3 },
                    { key: 'PersoV4', frame: 2 },
                    { key: 'PersoV4', frame: 1 },
                ],
	    frameRate: 14,
	    repeat: -1
	});

	this.anims.create({
	    key: 'turn',
	    frames: [ { key: 'PersoV4', frame: 1 } ],
	    frameRate: 10
	});

	this.anims.create({
		key: 'run',
		frames: this.anims.generateFrameNumbers('enemy', { start: 0, end: 3}),
		frameRate: 13
	});

	

	/*this.anims.create({
	    key: 'right',
	    frames: this.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
	    frameRate: 10,
	    repeat: -1
	});*/


	//On crée les fruits à récupérer
	fruits = this.physics.add.group();
	    //key: ['cerise','fraise','asperge','citron'],
	    //repeat: 3,
	    //setXY: { x: 350, y:0, stepX: Phaser.Math.Between(200, 500)}
	//});
	fruits.create(400,0,'raisin1');
	fruits.create(500,0,'cerise');
	fruits.create(700,0,'fraise');
	fruits.create(900,0,'asperge');
	fruits.create(1100,0,'citron');
	fruits.create(1600,0,'banane');
	fruits.create(1800,0,'ananas');
	fruits.create(2200,0,'cerise');
	fruits.create(2400,0,'ananas');
	fruits.create(3000,0,'cerise');
	fruits.create(3600,0,'raisin1');
	fruits.create(3800,0,'fraise');
	fruits.children.iterate(function(child){
	    child.setBounceY(Phaser.Math.FloatBetween(0.2, 0.4));
	});
	this.physics.add.collider(fruits, platforms);
	this.physics.add.overlap(player, fruits, collectFruits, null, this);

	BadFood = this.physics.add.group();
	    //key: ['hamburger', 'sandwich', 'frites'],
	    //repeat: 1,
	    //setXY: { x: 1000, y:0, stepX: Phaser.Math.Between(1000, 2000)}
	//});
	BadFood.create(1300,0,'hamburger');
	BadFood.create(1400,0,'sandwich');
	BadFood.create(1700,600,'frites');
	BadFood.create(1900,600,'sandwich');
	BadFood.create(2100,600,'hamburger');
	BadFood.create(2500,600,'frites');
	BadFood.create(2800,600,'sandwich');
	BadFood.create(2180,600,'hamburger');
	BadFood.create(3200,0,'sandwich');
	BadFood.create(3400,0,'sandwich');
	BadFood.children.iterate(function(child){
	    child.setBounceY(Phaser.Math.FloatBetween(0.2, 0.4));
	});
	this.physics.add.collider(BadFood, platforms);
	this.physics.add.overlap(player, BadFood, collectBadFood, null, this);


	function collectFruits (player, fruits){
	    fruits.disableBody(true,true);
	    vitamines += 100;
	    calories += 300;
	    vitaminesCounter.setText('vitamines: '+ vitamines);
	    matièresGCounter.setText('matièresG :'+ matièresG);
	    this.caloriesCounter.setText('calories :'+ calories);
	    /*if (fruits.countActive(true) === 0)
	    {
	        fruits.children.iterate(function (child) {

	            child.enableBody(true, child.x, 0, true, true);

	        });
.setScale(0.5).refreshBody();
	        var x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

	        var bomb = bombs.create(x, 16, 'bomb');
	        bomb.setBounce(1);
	        bomb.setCollideWorldBounds(true);
	        bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);

	    }*/
	}
	function collectBadFood (player,BadFood){
		BadFood.disableBody(true,true);
	    matièresG += 100;
	    calories += 1500;
	    vitaminesCounter.setText('vitamines: '+ vitamines);
	    matièresGCounter.setText('matièresG :'+ matièresG);
	    this.caloriesCounter.setText('calories :'+ calories);
	}
	function startChrono(player){
		if (TimerOn == 0) {
		this.timer = this.time.addEvent({ delay: chrono, callback: gameOver, callbackScope: this });
	    this.TimeInfo = this.add.text(630, 80, '',{ font: '32px Calibri', fill: '#FFFAFA' }).setScrollFactor(0);
	    TimerOn = 1;
	}
	}
	function boxkey(player,box){
		box.disableBody(true, true);
		boxUsed.create(3600,300,'boxItem_disabled').setScale(0.5).refreshBody();
		key.create(5500,700,'keyRed');
	}
	function doorkey(player,key){
		key.disableBody(true, true);
		keyFound = 1;
	}
	function doorUnlock(player,doorClosed){
		if (keyFound == 1) {
			doorClosed.disableBody(true,true);
			doorOpen.create(6400,692, 'doorOpenmid').setScale(0.7);
	    	doorOpen.create(6400,624,'doorOpentop').setScale(0.7);
		}
	}
	function EnemyAPP(player, boxEnemy){
		boxEnemy.disableBody(true,true);
		boxEnemyUsed.create(2400,100,'boxEnemyUsed').setScale(0.5).refreshBody();

		//DEBUT ennemies
	    enemy = this.physics.add.sprite(2900,100, 'enemy' );
		enemy.setBounce(0.2);
		enemy.setCollideWorldBounds(false);
		enemy.body.setGravityY(500);
		this.physics.add.collider(enemy, platforms);
		enemy.setCollideWorldBounds(false);
		this.physics.add.overlap(player,enemy,gameOver, null, this);
		//Fin ennemies

	}
	function starAnim(star,platforms){
		this.star.setVelocityY(-200);
	}
	function Bonus(player,star){
		star.disableBody(true,true);
		BonusFinal = 1;
	}
	function gameOver(){	
		this.physics.pause();
	    player.setTint(0xff0000);
	    player.anims.play('turn');
	    gameOver = true;
	    this.timer.paused = true;
	    this.add.image(650, 300,'gameover').setScale(0.8).setScrollFactor(0);
        let bouton = this.add.image(650, 380, 'bHome').setScale(0.08).setInteractive().setScrollFactor(0).on('pointerdown', () => {
            location.reload();
        });
	   }
	function victory(){
		this.physics.pause();
		this.timer.paused = true;
		vitaminesR = this.add.text(700,340, vitamines, { fontSize: '48px Calibri'}).setScrollFactor(0).setDepth(100);
		matieresGR = this.add.text(750,410, matièresG,{fontSize: '48px Calibri'}).setScrollFactor(0).setDepth(100);
		caloriesR = this.add.text(700,500, calories, {fontSize: '48px Calibri'}).setScrollFactor(0).setDepth(100);
		let resultats = this.add.image(700, 400, 'resultatTab').setInteractive().setScrollFactor(0).on('pointerdown', () => {
            resultats.destroy();
            vitaminesR.destroy();
            matieresGR.destroy();
            caloriesR.destroy();
            let victoire = this.add.image(650, 300,'victory').setScale(0.8).setScrollFactor(0);
            let bouton = this.add.image(650, 380, 'next').setInteractive().setScrollFactor(0).on('pointerdown', () => {
            this.scene.start('level 2');
        });
        });

		/*this.add.image(650, 300,'victory').setScale(0.8).setScrollFactor(0);
        let bouton = this.add.image(650, 380, 'next').setInteractive().setScrollFactor(0).on('pointerdown', () => {
            this.scene.start('level 2');
        });*/
	}

	/*bombs = this.physics.add.group();
	this.physics.add.collider(player, bombs, hitBomb, null, this);
	this.physics.add.collider(bombs, platforms);

	function hitBomb (player, bomb){
	    this.physics.pause();
	    player.setTint(0xff0000);
	    player.anims.play('turn');
	    gameOver = true;
	}*/
	}

	update ()
	{

	/*Animations IA*/
	if (enemy) {
		enemy.anims.play('run', true);
		enemy.setVelocityX(-340);
		enemy.flipX = true;
	}

	/*Gestion de l'étoile bonus et de son animation*/
	if (vitamines >= 600 && BonusACT == 0) {
	    	//this.star.create(3400,600,'star');
	    	BonusACT = 1;
	    }


	/*Gestion timer*/
	if(this.TimeInfo){
	this.TimeInfo.setText('Time: ' + Math.floor(chrono - this.timer.getElapsed())/1000);
		if (chrono - this.timer.getElapsed() < 10) {
			this.TimeInfo.setFill('#FF0921');
		}
		else{
			this.TimeInfo.setFill('#FFFAFA');
		}
	}
		/*Gestion des objectifs*/
	if (vitamines >= 1100) {
		vitaminesCounter.setFill('#00FF00');
	}
	else if (vitamines != 0 && vitamines < 1100) {
		vitaminesCounter.setFill('#FFFAFA');
	}
	else if (vitamines == 0) {
		vitaminesCounter.setFill('#FF0921');
	}
	if (matièresG > 300) {
		matièresGCounter.setFill('#FF0921');
	}
	else if (matièresG <= 300) {
		matièresGCounter.setFill('#00FF00');
	}
	if (calories > 5000) {
		this.caloriesCounter.setFill('#FF0921');
	}
	else if (calories != 0 && calories <= 5000) {
		this.caloriesCounter.setFill('#FFFAFA');
	}
	else if (calories == 0) {
		this.caloriesCounter.setFill('#FF0921');
	}
	/*Gestion déplacements*/
		if (vitamines >= 0 && vitamines < 500 && calories != 0){
			if (cursors.Q.isDown || cursors.left.isDown)
			{
			    player.setVelocityX(-240);

			    player.anims.play('walk', true);

			    player.flipX = true;
			    calories -= 1;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else if (cursors.D.isDown || cursors.right.isDown)
			{
			    player.setVelocityX(240);

			    player.anims.play('walk', true);

			    player.flipX = false;

			    calories -= 1;
			  if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else
			{
			    player.setVelocityX(0);

			    player.anims.play('turn');
			}

			if (cursors.space.isDown && player.body.touching.down || cursors.Z.isDown && player.body.touching.down)
			{
			    player.setVelocityY(-400);
			    calories -= 5;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
		}

		else if (vitamines >= 500 && BonusFinal == 0 && calories != 0){
			if (cursors.Q.isDown || cursors.left.isDown)
			{
			    player.setVelocityX(-340);

			    player.anims.play('walk', true);

			    player.flipX = true;
			    calories -= 1;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else if (cursors.D.isDown || cursors.right.isDown)
			{
			    player.setVelocityX(340);

			    player.anims.play('walk', true);

			    player.flipX = false;

			    calories -= 1;
			  if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else
			{
			    player.setVelocityX(0);

			    player.anims.play('turn');
			}

			if (cursors.space.isDown && player.body.touching.down || cursors.Z.isDown && player.body.touching.down)
			{
			    player.setVelocityY(-580);
			    calories -= 5;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
		}

		 else if (BonusFinal == 1){
			if (cursors.Q.isDown || cursors.left.isDown)
			{
			    player.setVelocityX(-340);

			    player.anims.play('walk', true);

			    player.flipX = true;
			    calories -= 1;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else if (cursors.D.isDown || cursors.right.isDown)
			{
			    player.setVelocityX(340);

			    player.anims.play('walk', true);

			    player.flipX = false;

			    calories -= 1;
			  if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else
			{
			    player.setVelocityX(0);

			    player.anims.play('turn');
			}

			if (cursors.space.isDown && player.body.touching.down || cursors.Z.isDown && player.body.touching.down)
			{
			    player.setVelocityY(-830);
			    calories -= 5;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
		}


		else{
			if (cursors.Q.isDown || cursors.left.isDown)
			{
			    player.setVelocityX(-200);

			    player.anims.play('walk', true);

			    player.flipX = true;

			    calories -= 1;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}
			else if (cursors.D.isDown || cursors.right.isDown)
			{
			    player.setVelocityX(200);

			    player.anims.play('walk', true);

			    player.flipX = false;

			    calories -= 1;

			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}

			}
			else
			{
			    player.setVelocityX(0);

			    player.anims.play('turn');
			}

			if (cursors.space.isDown && player.body.touching.down || cursors.Z.isDown && player.body.touching.down)
			{
			    player.setVelocityY(-100);

			    calories -= 5;
			    if (calories > 0) {
			    this.caloriesCounter.setText('calories :' + calories);
				}
				else{
					calories = 0;
					this.caloriesCounter.setText('calories :' + calories);
				}
			}			
		}
	}
}