const path = require('path');

module.exports = {
  entry: {
      app: './src/index.js',
      vendor: 'phaser'
  },
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
    chunkFilename: 'vendor.js'
  },
  optimization: {
    splitChunks: {
        name: 'vendor',
        chunks: 'all'
    }
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  },
  watch: true
};
