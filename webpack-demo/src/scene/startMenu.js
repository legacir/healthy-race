import Phaser from 'phaser';

    var sprites = [];
    var food = ['cerise','fraise','asperge','citron','frites','raisin1','hamburger','sandwich'];
    var index = 0;
    var platforms;
    var enemy;
    var donut;
    var FKey;
    let canvas;
    let fullscreen;

export default class extends Phaser.Scene
{

    constructor (){
        super({key: 'startMenu'});
    }
    preload(){}

    create (){
        this.add.image(650,300,'sky').setScale(2);
        /*let bouton = this.add.image(950, 180, 'pleinEcran').setScale(0.5).setInteractive().setScrollFactor(0).on('pointerdown', () => {
            this.canvas[this.fullscreen.request]();
        });*/

        //  Create the particles
        for (var i = 0; i < 200; i++){
            var x = Phaser.Math.Between(-200, 1400);
            var y = Phaser.Math.Between(-64, 900);
            var image = this.add.image(x, y, food[index]);
            if (index == 7) {
                index = 0;
            }
            else{
                index++;
            }
            //image.setBlendMode(Phaser.BlendModes.ADD);
            sprites.push({ s: image, r: 2 + Math.random() * 6 });
        }
        this.add.image(60,375,'PersoPret');
        this.add.image(650, 200, 'titre').setBlendMode(Phaser.BlendModes.DIFFERENCE).setScale(0.8);
        let boutonPlay = this.add.image(650, 300, 'bPlay1').setScale(1).setInteractive().on('pointerdown', () => {
            this.scene.start('level 0');
        });
        let boutonOption = this.add.image(650, 400, 'bOption').setScale(1).setInteractive().on('pointerdown', () => {
            //a coder
        });
        let boutonCredit = this.add.image(650, 500, 'bCredit').setScale(1).setInteractive().on('pointerdown', () => {
            //a coder
        });


        var music = this.sound.add('musiqueMenu', {loop: true});
        //for(var i = 0;i<10;i++){
            music.play();
        //}

        FKey = this.input.keyboard.addKeys('F');

        platforms = this.physics.add.staticGroup();
        for(var x = -56; x <= 72; x = x +128){
            for(var j = 600; j <= 1028; j = j + 128){
                platforms.create(x,j,'terre');
            }
        }
        for(var x = -56; x <= 72; x = x + 128){
            platforms.create(x,472,'sol2');
        }
        for(var i = 200; i <= 1096; i = i +128){
            platforms.create(i, 758, 'sol2');
        }
        for(var x = 1224; x <= 1352; x = x +128){
            for(var j = 600; j <= 1028; j = j + 128){
                platforms.create(x,j,'terre');
            }
        }
        for(var x = 1224; x <= 1352; x = x + 128){
            platforms.create(x,472,'sol2');
        }

        enemy = this.physics.add.sprite(300,650, 'enemy' );
        enemy.setBounce(0.2);
        enemy.setCollideWorldBounds(false);
        enemy.body.setGravityY(500);
        enemy.setVelocityX(140);
        this.physics.add.collider(enemy, platforms);
        enemy.setCollideWorldBounds(false);

        donut = this.physics.add.sprite(900,650, 'donut' );
        donut.setBounce(0.2);
        donut.setCollideWorldBounds(false);
        donut.body.setGravityY(500);
        donut.setVelocityX(140);
        this.physics.add.collider(donut, platforms);
        donut.setCollideWorldBounds(false);

        this.anims.create({
            key: 'run',
            frames: this.anims.generateFrameNumbers('enemy', { start: 0, end: 3}),
            frameRate: 13
        });
        this.anims.create({
            key: 'avance',
            frames: this.anims.generateFrameNumbers('donut', { start: 0, end: 3}),
            frameRate: 13
        });

    }
    update (time){
        /*if (FKey.F.isDown){

            if (this.scale.isFullscreen)
            {
                button.setFrame(0);
                this.scale.stopFullscreen();
            }
            else
            {
                button.setFrame(1);
                this.scale.startFullscreen();
            }

        }*/

        enemy.anims.play('run', true);
        donut.anims.play('avance', true);

        if(enemy.x < 220){
        enemy.setVelocityX(140);
        enemy.flipX = false;
        }


        if(enemy.x > 1064){
            enemy.setVelocityX(-140);
            enemy.flipX = true;
        }
        if(donut.x < 220){
            donut.setVelocityX(140);
            donut.flipX = false;
        }


        if(donut.x > 1064){
            donut.setVelocityX(-140);
            donut.flipX = true;
        }
        for (var i = 0; i < sprites.length; i++){
            var sprite = sprites[i].s;
            sprite.y -= sprites[i].r;
            if (sprite.y < -256)
            {
                sprite.y = 900;
            }
        }
    }
}

